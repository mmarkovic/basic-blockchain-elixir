defmodule BlockchainTest do
  use ExUnit.Case
  doctest Blockchain

  test "creates a new blockchain" do
    chain = Blockchain.new()
    assert is_list(chain)
    assert chain |> Enum.at(0) |> Map.get(:data) == "ZERO_DATA"
    assert chain |> Enum.at(0) |> Map.get(:prev_hash) == "ZERO_HASH"
  end

  test "inserts new data as a block" do
    chain = Blockchain.new()
    chain = Blockchain.insert(chain, "MESSAGE 1")
    assert is_list(chain)
    assert length(chain) == 2
    assert chain |> Enum.at(1) |> Map.get(:data) == "ZERO_DATA"
    assert chain |> Enum.at(0) |> Map.get(:data) == "MESSAGE 1"
  end

  test "inserts a few blocks and validates the blockchain" do
    chain = Blockchain.new()

    chain =
      chain
      |> Blockchain.insert("MESSAGE 1")
      |> Blockchain.insert("MESSAGE 2")
      |> Blockchain.insert("MESSAGE 3")

    assert is_list(chain)
    assert length(chain) == 4
    assert chain |> Blockchain.valid?()
  end
end
